Require Import World.
Require Import AxiomaticIO.

Module UnitWorld
<: World
.

Definition msg := unit.

End UnitWorld.

Module AxiomaticIOInAUnitWorld.
Include AxiomaticIO UnitWorld.
End AxiomaticIOInAUnitWorld.
