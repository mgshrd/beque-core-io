Require Extraction.

Require Import World.
Require Import IORaw.
Require Import DelayExtractionDefinitionToken.


(** Axomatization of the IO monad, using less complex axioms than the one in [AxiomaticIO]'s [IO_raw_bind] *)
Module AxiomaticIOSmallAxioms
       (w : World)
<: IORaw w
.

(** Hoareified IO monad *)
Axiom IO :
  forall
    (pre : list w.msg -> Prop)
    (T : Set)
    (post : T -> (*addition*) list w.msg -> (*post trace*) list w.msg -> Prop),
    Set.

(** Allow binding an action after another if the postcondition of the base action enables the the precondition of the second action. *)
Axiom IO_raw_simple_bind :
  forall
    {pre_a A post_a}
    (ma : IO pre_a A post_a)
    {B post_b}
    (f : forall a : A, IO (fun t => exists s t', (s ++ t')%list = t /\ post_a a s t') B (post_b a)),
    IO pre_a B (fun b s t => exists a s'' s', s'' ++ s' = s /\ post_b a b s'' (s' ++ t) /\ post_a a s' t)%list.

(** Allow binding an action after another if the postcondition of the base action enables the the precondition of the second action. Carry the input, useful for dependent result types. *)
Axiom IO_raw_simple_dep_bind :
  forall
    {pre_a A post_a}
    (ma : IO pre_a A post_a)
    {B : A -> Set} {post_b}
    (f : forall a : A, IO (fun t => exists s t', (s ++ t')%list = t /\ post_a a s t') (B a) (post_b a)),
    IO pre_a (sigS B) (fun ab s t => exists s'' s', s'' ++ s' = s /\ post_b (projT1 ab) (projT2 ab) s'' (s' ++ t) /\ post_a (projT1 ab) s' t)%list.

(** Wrap a value in an action, that does not change the guarantees about the trace *)
Axiom IO_raw_return :
  forall
    {A : Set} (a : A),
    IO (fun _ => True) A (fun a' s t => a' = a /\ s = nil).

(** Replace precondition with a stronger one. *)
Axiom IO_raw_strengthen :
  forall
    {pre_a A post_a}
    (ma : IO pre_a A post_a)
    (pre_a' : list w.msg -> Prop)
    (impl_pre : forall t, pre_a' t -> pre_a t),
    IO pre_a' A post_a.

(** Replace postcondition with a weaker one. *)
Axiom IO_raw_weaken :
  forall
    {pre_a A post_a}
    (ma : IO pre_a A post_a)
    (post_a' : A -> list w.msg -> list w.msg -> Prop)
    (impl_post : forall a s t, pre_a t -> post_a a s t -> post_a' a s t),
    IO pre_a A post_a'.

(** Express, that the postcondition receives the same [t] as the precondition.

Intuition: the past does not change, the same props are _not_ true that were true once, but they _were_ true once. *)
Axiom IO_raw_propagate_pre :
  forall
    {pre_a A post_a}
    (ma : IO pre_a A post_a),
    IO pre_a A (fun a s t => pre_a t /\ post_a a s t).

Definition IO_raw_bind :
  forall
    (pre : list w.msg -> Prop) (* facts about the world *)
    {pre_a A post_a}
    (ma : IO pre_a A post_a)
    (impl_pre :
       forall t,
         pre t ->
         pre_a t)
    {pre_b B post_b}
    (f : forall a : A, IO (pre_b a) B (post_b a))
    (impl_mid :
       forall a s t,
         pre t ->
         pre_a t ->
         post_a a s t ->
         pre_b a (s ++ t)%list)
    (post : B -> list w.msg -> list w.msg -> Prop)
    (impl :
       forall b s'' a s' t,
         pre t -> (* propagate truths unrelated to [ma] and [f]*)
         pre_a t ->
         post_a a s' t ->
         pre_b a (s' ++ t)%list ->
         post_b a b s'' (s' ++ t)%list ->
         post b (s'' ++ s')%list t),
    IO pre B post.
Proof.
  intros.
  assert (Hx := IO_raw_strengthen ma pre impl_pre).
  assert (Hy := IO_raw_propagate_pre Hx); clear Hx.
  unshelve eassert (Hz := IO_raw_simple_bind Hy (fun a => IO_raw_strengthen (f a) _ _)); clear Hy.
  { intros. simpl in *. destruct H as [ ? [ ? [ ? [ ? ? ] ] ] ]. subst t. apply impl_mid; auto. }
  assert (Ha := IO_raw_weaken Hz post); clear Hz.
  simpl in Ha; apply Ha; clear Ha.
  intros. destruct H0 as [ ? [ ? [ ? ? ] ] ]. destruct H0 as [ ? [ ? [ ? ? ] ] ].
  subst s. refine (impl _ _ _ _ _ H _ H3 _ H1).
  - apply impl_pre. assumption.
  - refine (impl_mid _ _ _ H _ H3).
    apply impl_pre. assumption.
Defined.

Definition IO_raw_projT1 :
  forall
    {pre} {X : Set} {A : X -> Set} {post_xa}
    (ma : IO pre (sigS A) post_xa)
    (post_x : X -> list w.msg -> list w.msg -> Prop)
    (impl : forall xa s t, post_xa xa s t -> post_x (projT1 xa) s t),
    IO pre X post_x.
Proof.
  intros.
  refine (IO_raw_bind _ ma _ (fun xa => IO_raw_return (projT1 xa)) _ _ _); [ solve [ auto ] | solve [auto ] | ].
  intros. destruct H3. subst. simpl. apply impl. assumption.
Defined.

Definition IO_raw_projT2 :
  forall
    {pre} {X : Set} {A : Set} {post_xa}
    (ma : IO pre (sigS (fun (_ : X) => A)) post_xa)
    (post_a : A -> list w.msg -> list w.msg -> Prop)
    (impl : forall xa s t, post_xa xa s t -> post_a (projT2 xa) s t),
    IO pre A post_a.
Proof.
  intros.
  apply (IO_raw_bind pre ma ltac:(auto) (fun xa => IO_raw_return (projT2 xa))).
  - auto.
  - intros. destruct H3. subst. simpl. apply impl. assumption.
Defined.

Module extractHaskell (delay : DelayExtractionDefinitionToken).
Extract Constant IO "a" => "Prelude.IO a".
Extract Inlined Constant IO_raw_simple_bind => "(Prelude.>>=)".
Extract Inlined Constant IO_raw_return => "(Prelude.return)".

Extract Inlined Constant IO_raw_propagate_pre => "{-propagate_pre-}".
Extract Inlined Constant IO_raw_strengthen => "{-strengthen-}".
Extract Inlined Constant IO_raw_weaken => "{-weaken-}".
End extractHaskell.

Module extractOcaml (delay : DelayExtractionDefinitionToken).
Extract Constant IO "'a" => "unit -> 'a".
Extract Constant IO_raw_simple_bind => "(fun v f -> (fun () -> (f (v ()))()))".
Extract Constant IO_raw_return => "(fun x -> (fun () -> x))".

Extract Constant IO_raw_propagate_pre => "(fun x -> x)".
Extract Constant IO_raw_strengthen => "(fun x -> x)".
Extract Constant IO_raw_weaken => "(fun x -> x)".
End extractOcaml.

End AxiomaticIOSmallAxioms.
