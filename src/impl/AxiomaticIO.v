Require Extraction.

Require Import IORaw.
Require Import World.
Require Import DelayExtractionDefinitionToken.

(* [IORaw] implemented with a strong [bind]. (TODO [IO_raw_strengthen]&[IO_raw_weaken] are the consequences of [IO_raw_bind] in [IORaw] as well.)*)
Module AxiomaticIO
       (w : World)
<: IORaw w
.

Axiom IO :
  forall
    (pre : list w.msg -> Prop)
    (T : Set)
    (post : T -> list w.msg -> list w.msg -> Prop),
    Set.

Axiom IO_raw_bind :
  forall
    (pre : list w.msg -> Prop)
    {pre_a A post_a}
    (ma : IO pre_a A post_a)
    (impl_pre :
       forall t,
         pre t ->
         pre_a t)
    {pre_b B post_b}
    (f : forall a : A, IO (pre_b a) B (post_b a))
    (impl_mid :
       forall a s t,
         pre t ->
         pre_a t ->
         post_a a s t ->
         pre_b a (s ++ t)%list)
    (post : B -> list w.msg -> list w.msg -> Prop)
    (impl :
       forall b s'' a s' t,
         pre t ->
         pre_a t ->
         post_a a s' t ->
         pre_b a (s' ++ t)%list ->
         post_b a b s'' (s' ++ t)%list ->
         post b (s'' ++ s')%list t ),
    IO pre B post.

Axiom IO_raw_return :
  forall
    {A : Set} a,
    IO (fun _ => True) A (fun a' s _ => a' = a /\ s = nil).

Definition IO_raw_strengthen :
  forall
    {pre_a A post_a}
    (ma : IO pre_a A post_a)
    {pre_a' : list w.msg -> Prop}
    (impl_pre : forall t, pre_a' t -> pre_a t),
    IO pre_a' A post_a.
Proof.
  intros. refine (IO_raw_bind _ ma _ IO_raw_return _ _ _); [ tauto | tauto | ].
  intros. destruct H3. subst. simpl. assumption.
Defined.

(** Replace postcondition with a weaker one. *)
Definition IO_raw_weaken :
  forall
    {pre_a A post_a}
    (ma : IO pre_a A post_a)
    {post_a' : A -> list w.msg -> list w.msg -> Prop}
    (impl_post : forall a s t, pre_a t -> post_a a s t -> post_a' a s t),
    IO pre_a A post_a'.
Proof.
  intros. refine (IO_raw_bind _ ma _ IO_raw_return _ _ _); [ tauto | tauto | ].
  intros. destruct H3. subst. simpl. apply impl_post; assumption.
Defined.

Definition IO_raw_projT1 :
  forall
    {pre} {X : Set} {A : X -> Set} {post_xa}
    (ma : IO pre (sigS A) post_xa)
    (post_x : X -> list w.msg -> list w.msg -> Prop)
    (impl : forall xa s t, post_xa xa s t -> post_x (projT1 xa) s t),
    IO pre X post_x.
Proof.
  intros.
  refine (IO_raw_bind _ ma _ (fun xa => IO_raw_return (projT1 xa)) _ _ _); [ solve [ auto ] | solve [auto ] | ].
  intros. destruct H3. subst. simpl. apply impl. assumption.
Defined.

Definition IO_raw_projT2 :
  forall
    {pre} {X : Set} {A : Set} {post_xa}
    (ma : IO pre (sigS (fun (_ : X) => A)) post_xa)
    (post_a : A -> list w.msg -> list w.msg -> Prop)
    (impl : forall xa s t, post_xa xa s t -> post_a (projT2 xa) s t),
    IO pre A post_a.
Proof.
  intros.
  apply (IO_raw_bind pre ma ltac:(auto) (fun xa => IO_raw_return (projT2 xa))).
  - auto.
  - intros. destruct H3. subst. simpl. apply impl. assumption.
Defined.

Module extractHaskell (delay : DelayExtractionDefinitionToken).
Extract Constant IO "a" => "Prelude.IO a".
Extract Inlined Constant IO_raw_bind => "(Prelude.>>=)".
Extract Inlined Constant IO_raw_return => "(Prelude.return)".
End extractHaskell.

Module extractOcaml (delay : DelayExtractionDefinitionToken).
Extract Constant IO "'a" => "unit -> 'a".
Extract Constant IO_raw_bind => "(fun v f -> (fun () -> (f (v ()))()))".
Extract Constant IO_raw_return => "(fun x -> (fun () -> x))".
End extractOcaml.

End AxiomaticIO.
