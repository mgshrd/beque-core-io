(** * Everything outside the program; the program can exchange [msg]es with the world. *)
Module Type World
.

(** The set of messages this world knows. *)
Parameter msg :
  Set.

End World.
