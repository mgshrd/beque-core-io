Require Import ltac_utils.
Require Import sigS.

Require Import World.
Require Import IORaw.

Module IOAux0
       (w : World)
       (ior : IORaw0 w)
.

Import ior.
Export sigS.

Definition IO_bind
           (pre : list w.msg -> Prop)
           {pre_a} {A : Set} {post_a}
           (ma : IO pre_a A post_a)
           (impl_pre :
              forall t,
                pre t ->
                pre_a t)
           {pre_b : A -> list w.msg -> Prop}
           {B : A -> Set}
           {post_b : forall a, B a -> list w.msg -> list w.msg -> Prop}
           (f : forall a : A, IO (pre_b a) (B a) (post_b a))
           (impl_mid :
              forall a s t,
                pre t ->
                pre_a t ->
                post_a a s t ->
                pre_b a (s ++ t)%list)
           (post : {a : A & B a} -> list w.msg -> list w.msg -> Prop)
           (impl :
              forall ab s'' s' t,
                pre t ->
                pre_a t ->
                post_a (projT1 ab) s' t ->
                pre_b (projT1 ab) (s' ++ t)%list ->
                post_b (projT1 ab) (projT2 ab) s'' (s' ++ t)%list ->
                post ab (s'' ++ s')%list t) :
  IO pre (sigS B) post.
Proof.
  set (Hx := fun pf => @IO_raw_dep_bind pre _ _ _ ma impl_pre _ _ _ f pf post).
  apply Hx.
  - intros. apply impl_mid; assumption.
  - intros.
    destruct ab. simpl in *.
    apply impl; assumption.
Defined.

Definition IO_bind_flexpre
           (pre : list w.msg -> Prop)
           {pre_a} {A : Set} {post_a}
           (ma : IO pre_a A post_a)
           (impl_pre :
              forall t,
                pre t ->
                pre_a t)
           {B : A -> Set}
           {post_b : forall a : A, B a -> list w.msg -> list w.msg -> Prop}
           (f : forall a : A, IO (fun t => exists s t', (s ++ t')%list = t /\ pre t' /\ post_a a s t') (B a) (post_b a)) :
  IO
    pre
    (sigS B)
    (fun ab s t =>
       exists s'' s',
         (s'' ++ s')%list = s /\
         pre_a t /\
         post_a (projT1 ab) s' t /\
         post_b (projT1 ab) (projT2 ab) s'' (s' ++ t)%list).
Proof.
  refine (IO_bind pre ma impl_pre f _ _ _).
  - intros.
    exists s. exists t. tauto.
  - intros. exists s''. exists s'. tauto.
Defined.

Definition IO_bind_rigid
           {pre_a} {A : Set} {post_a}
           (ma : IO pre_a A post_a)
           {B : A -> Set}
           {post_b : forall a : A, B a -> list w.msg -> list w.msg -> Prop}
           (f : forall a : A, IO (fun t => exists s t', (s ++ t')%list = t /\ pre_a t' /\ post_a a s t') (B a) (post_b a)) :
  IO
    pre_a
    (sigS B)
    (fun ab s t =>
       exists s'' s',
         (s'' ++ s')%list = s /\
         pre_a t /\
         post_a (projT1 ab) s' t /\
         post_b (projT1 ab) (projT2 ab) s'' (s' ++ t)%list).
Proof.
  refine (IO_bind_flexpre _ ma _ f).
  auto.
Defined.

(** utility function to add requirements to a postcondition *)
Definition enrich_post
           {pre A post}
           (m : IO pre A post)
           (p : A -> list w.msg -> list w.msg -> Prop)
  := IO pre A (fun a s t => post a s t /\ p a s t).

(* make precondition stricter *)
Definition IO_strengthen
           {pre_a A post_a}
           (ma : IO pre_a A post_a)
           (pre_a' : list w.msg -> Prop)
           (impl_pre : forall t, pre_a' t -> pre_a t) :
  IO pre_a' A post_a := IO_raw_strengthen ma _ impl_pre.

(* make postcondition guarantee less *)
Definition IO_weaken
           {pre_a A post_a}
           (ma : IO pre_a A post_a)
           (post_a' : A -> list w.msg -> list w.msg -> Prop)
           (impl_post : forall a s t, pre_a t -> post_a a s t -> post_a' a s t) :
  IO pre_a A post_a' := IO_raw_weaken ma _ impl_post.

Definition IO_cmd
           (pre : list w.msg -> Prop)
           (post : list w.msg -> list w.msg -> Prop)
  := IO pre unit (fun _ => post).

Definition get_post
           {pre A post}
           (_ : IO pre A post)
  := post.

Definition IO_forgetful_bind
           (pre : list w.msg -> Prop)
           {pre_a} {A : Set} {post_a}
           (ma : IO pre_a A post_a)
           (impl_pre :
              forall t,
                pre t ->
                pre_a t)
           {pre_b : A -> list w.msg -> Prop}
           {B : Set}
           {post_b : B -> list w.msg -> list w.msg -> Prop}
           (f : forall a : A, IO (pre_b a) B post_b)
           (impl_mid :
              forall a s t,
                pre t ->
                pre_a t ->
                post_a a s t ->
                pre_b a (s ++ t)%list) :
  IO pre B (fun b s t =>
              pre t /\
              pre_a t /\
              exists s'' s',
                (s'' ++ s')%list = s /\
                (exists a, post_a a s' t /\ pre_b a (s' ++ t)%list) /\
                post_b b s'' (s' ++ t)%list).
Proof.
  set (Hx := @IO_raw_bind
               pre
               _ _ _
               ma
               impl_pre
               pre_b B (fun _ => post_b)
               f impl_mid).
  apply Hx.
  intros.
  split; [ assumption | ].
  split; [ assumption | ].
  exists s''. exists s'.
  split; [ reflexivity | ].
  split; [ | assumption ].
  exists a. split; assumption.
Defined.

Notation "/| m" := (IO_strengthen m _ _) (at level 30, m at next level).
Notation "|\ m" := (IO_weaken m _ _) (at level 30, m at next level).
Notation "a <== ma ;; e" := (IO_bind_rigid ma (fun a => e)) (at level 30, ma at next level, right associativity).
Notation "a <-- ma ;; e" := (IO_bind_flexpre _ ma _ (fun a => e)) (at level 30, ma at next level, right associativity).
Notation "'dor' x" := (x) (at level 30, right associativity).

(** analog to Ynot's case tactics, but intended as less of a special case *)
Section CaseHandling.

Definition sumbind
           (pre : list w.msg -> Prop)
           {A E : Set}
           (r : E+A)
           {B : A -> Set} {posta}
           (f : forall (a : A), IO (fun t => pre t /\ r = inr a) (B a) (posta a))
           {B' : E -> Set} {poste}
           (g : forall (e : E), IO (fun t => pre t /\ r = inl e) (B' e) (poste e)) :
    IO
      pre
      match r with
      | inr a => B a
      | inl e => B' e
      end
      (fun b s t =>
         match r as r' return r = r' -> _ with
         | inr a =>
           fun eqpf =>
             posta a ltac:(rewrite eqpf in b; exact b) s t /\
             r = inr a
         | inl e =>
           fun eqpf =>
             poste e ltac:(rewrite eqpf in b; exact b) s t /\
             r = inl e
         end eq_refl).
Proof.
  intros. destruct r.
  - refine (|\ /| g e); auto.
  - refine (|\ /| f a); auto.
Defined.

Definition sumbindsig
           (pre : list w.msg -> Prop)
           {A E : Set} {Da De : Set}
           (r : sigS (fun r' : E+A => match r' with
                                             | inr _ => Da
                                             | inl _ => De
                                             end))
           {B : A * Da -> Set} {post}
           (f : forall (a : A * Da), IO (fun t => pre t /\ r = existT _ (inr (fst a)) (snd a)) (B a) (post a))
           {B' : E * De -> Set} {post'}
           (g : forall (e : E * De), IO (fun t => pre t /\ r = existT _ (inl (fst e)) (snd e)) (B' e) (post' e)) :
    IO
      pre
      match r with
      | existT _ (inr a) d => B (a, d)
      | existT _ (inl e) d => B' (e, d)
      end
      (fun b s t =>
         match r as r' return r = r' -> _ with
         | existT _ (inr a) d =>
           fun eqpf =>
             post (a, d) ltac:(rewrite eqpf in b; exact b) s t /\
             projT1 r = inr a
         | existT _ (inl e) d =>
           fun eqpf =>
             post' (e, d) ltac:(rewrite eqpf in b; exact b) s t /\
             projT1 r = inl e
         end eq_refl).
Proof.
  intros. destruct r. destruct x; simpl in *.
  - refine (|\ /| g (e, y)); intros.
    + split; [ | reflexivity ]. assumption.
    + split; [ | reflexivity ]. assumption.
  - refine (|\ /| f (a, y)); intros; split; try reflexivity; assumption.
Defined.


End CaseHandling.

End IOAux0.


Module IOAux
       (w : World)
       (ior : IORaw w)
.

Import ior.
Export sigS.

Module ior0 := IORaw2IORaw0 w ior.
Module aux0 := IOAux0 w ior0.
Export aux0.

Definition IO_return
           {A : Set} a :
  IO (fun _ => True) A (fun a' s t => a' = a /\ s = nil) := IO_raw_return a.

Definition IO_pure_map
           {pre} {A : Set} {post_a}
           (ma : IO pre A post_a)
           {B : Set}
           (f : A -> B)
           (post_b : B -> list w.msg -> list w.msg -> Prop)
           (impl : forall a s t, pre t -> post_a a s t -> post_b (f a) s t) :
  IO pre B post_b.
Proof.
  refine (IO_raw_bind pre ma _ (fun a => IO_return (f a)) _ post_b _).
  { auto. }
  { auto. }
  { intros. destruct H3. subst. simpl. apply impl; assumption. }
Defined.

Definition IO_drop_value
           {pre A post_a}
           (m : IO pre A post_a)
           (post : list w.msg -> list w.msg -> Prop)
           (impl : forall a s t, pre t -> post_a a s t -> post s t) :
  IO_cmd pre post.
Proof.
  exact (IO_pure_map m (fun _ => tt) (fun _ => post) impl).
Defined.

Notation "'do' x" := (IO_pure_map x (fun v => _) _ _) (at level 30, right associativity).
Notation "'dof' x 'funx' r '=>' f" := (IO_pure_map x (fun r => f) _ _) (at level 30, right associativity).
Notation "'dou' x" := (IO_drop_value x _ _) (at level 30, right associativity).

End IOAux.
