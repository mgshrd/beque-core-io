Require Import sigS.

Require Import World.

(** * IO Action types *)

(** [IORawRaw] IO only interface (i.e. [return]/[pure]/[eval] is not part of the interface.) *)
Module Type IORawRaw
       (w : World)
.

(** The hoarified IO monad.
 - pre: this action is only available, if the trace fulfills this predicate
 - T: evalutating this action produces a value of this type
 - post: after evaluating this action, the produced value and the resulting trace fulfills this predicate
 *)
Parameter IO :
  forall
    (pre : list w.msg -> Prop)
    (T : Set)
    (post : T -> (*addition*) list w.msg -> (*post trace*) list w.msg -> Prop),
    Set.

(** Producing a new action by applying an action producing function [f] to the result of the action [ma].

 Only allowed if the action produced by the function [f] is guaranteed to be enabled after evaluating [ma], that is, the precondition is true, regardless which [A] value is produced by [ma].
 *)
Parameter IO_raw_bind :
  forall
    (pre : list w.msg -> Prop) (* facts about the world *)
    {pre_a A post_a}
    (ma : IO pre_a A post_a)
    (impl_pre :
       forall t,
         pre t ->
         pre_a t)
    {pre_b B post_b}
    (f : forall a : A, IO (pre_b a) B (post_b a))
    (impl_mid :
       forall a s t,
         pre t ->
         pre_a t ->
         post_a a s t ->
         pre_b a (s ++ t)%list)
    (post : B -> list w.msg -> list w.msg -> Prop)
    (impl :
       forall b s'' a s' t,
         pre t -> (* propagate truths unrelated to [ma] and [f]*)
         pre_a t ->
         post_a a s' t ->
         pre_b a (s' ++ t)%list ->
         post_b a b s'' (s' ++ t)%list ->
         post b (s'' ++ s')%list t),
    IO pre B post.

(** Replace precondition with a stronger one. *)
Parameter IO_raw_strengthen :
  forall
    {pre_a A post_a}
    (ma : IO pre_a A post_a)
    (pre_a' : list w.msg -> Prop)
    (impl_pre : forall t, pre_a' t -> pre_a t),
    IO pre_a' A post_a.

(** Replace postcondition with a weaker one. *)
Parameter IO_raw_weaken :
  forall
    {pre_a A post_a}
    (ma : IO pre_a A post_a)
    (post_a' : A -> list w.msg -> list w.msg -> Prop)
    (impl_post : forall a s t, pre_a t -> post_a a s t -> post_a' a s t),
    IO pre_a A post_a'.

End IORawRaw.

(** [IORaw0] is the extension of IORawRaw with [IO_raw_dep_bind] *)
Module Type IORaw0
       (w : World)
<: IORawRaw w
.

Include IORawRaw w.

(** Producing a new action by applying an action producing function [f] to the result of the action [ma].

 Only allowed if the action produced by the function [f] is guaranteed to be enabled after evaluating [ma], that is, the precondition is true, regardless which [A] value is produced by [ma].

 Keeps track of the input value, for cases when the result type depends on the input value.
 *)
Parameter IO_raw_dep_bind :
  forall
    (pre : list w.msg -> Prop) (* facts about the world *)
    {pre_a A post_a}
    (ma : IO pre_a A post_a)
    (impl_pre :
       forall t,
         pre t ->
         pre_a t)
    {pre_b} {B : A -> Set} {post_b}
    (f : forall a : A, IO (pre_b a) (B a) (post_b a))
    (impl_mid :
       forall a s t,
         pre t ->
         pre_a t ->
         post_a a s t ->
         pre_b a (s ++ t)%list)
    (post : sigS B -> list w.msg -> list w.msg -> Prop)
    (impl :
       forall ab s'' s' t,
         pre t -> (* propagate truths unrelated to [ma] and [f]*)
         pre_a t ->
         post_a (projT1 ab) s' t ->
         pre_b (projT1 ab) (s' ++ t)%list ->
         post_b (projT1 ab) (projT2 ab) s'' (s' ++ t)%list ->
         post ab (s'' ++ s')%list t),
    IO pre (sigS B) post.

Parameter IO_raw_projT1 :
  forall
    {pre} {X : Set} {A : X -> Set} {post_xa}
    (ma : IO pre (sigS A) post_xa)
    (post_x : X -> list w.msg -> list w.msg -> Prop)
    (impl : forall xa s t, post_xa xa s t -> post_x (projT1 xa) s t),
    IO pre X post_x.

Parameter IO_raw_projT2 :
  forall
    {pre} {X : Set} {A : Set} {post_xa}
    (ma : IO pre (sigS (fun (_ : X) => A)) post_xa)
    (post_a : A -> list w.msg -> list w.msg -> Prop)
    (impl : forall xa s t, post_xa xa s t -> post_a (projT2 xa) s t),
    IO pre A post_a.

End IORaw0.

(** [IORaw] is the extension of IORawRaw with [return] *)
Module Type IORaw
       (w : World)
<: IORawRaw w
.

Include IORawRaw w.

(** Wrap a value in an action, that does not change the guarantees about the trace *)
Parameter IO_raw_return :
  forall
    {A : Set} (a : A),
    IO (fun _ => True) A (fun a' s t => a' = a /\ s = nil).

End IORaw.

(** Implement [IO_raw_dep_bind] in terms of [IO_raw_return] *)
Module IORaw2IORaw0
       (w : World)
       (ior : IORaw w)
<: IORaw0 w
.

Include ior.

Definition push_down_dependency
           {A : Set}
           {pre}
           {B : A -> Set}
           {post : forall a : A, B a -> list w.msg -> list w.msg -> Prop}
           (f : forall a : A, IO (pre a) (B a) (post a)) :
  forall a,
    IO (pre a) (sigS B) (fun ab s t => projT1 ab = a /\ post (projT1 ab) (projT2 ab) s t).
Proof.
  intros.
  refine (IO_raw_bind (pre a) (f a) ltac:(auto) (fun b => IO_raw_return (existT _ a b)) ltac:(simpl; auto) _ _).
  intros.
  destruct H2 as [ beq t'eq ]. destruct H3. subst b. subst s''.
  simpl in *. exact (conj eq_refl H1).
Defined.

Definition IO_raw_dep_bind :
  forall
    (pre : list w.msg -> Prop) (* facts about the world *)
    {pre_a A post_a}
    (ma : IO pre_a A post_a)
    (impl_pre :
       forall t,
         pre t ->
         pre_a t)
    {pre_b} {B : A -> Set} {post_b}
    (f : forall a : A, IO (pre_b a) (B a) (post_b a))
    (impl_mid :
       forall a s t,
         pre t ->
         pre_a t ->
         post_a a s t ->
         pre_b a (s ++ t)%list)
    (post : sigS B -> list w.msg -> list w.msg -> Prop)
    (impl :
       forall ab s'' s' t,
         pre t -> (* propagate truths unrelated to [ma] and [f]*)
         pre_a t ->
         post_a (projT1 ab) s' t ->
         pre_b (projT1 ab) (s' ++ t)%list ->
         post_b (projT1 ab) (projT2 ab) s'' (s' ++ t)%list ->
         post ab (s'' ++ s')%list t),
    IO pre (sigS B) post.
Proof.
  intros.
  set (Hx := push_down_dependency f).
  refine (IO_raw_bind _ ma _ Hx _ _ _); [ solve [ auto ] | solve [ auto ] | ].
  intros. destruct b. simpl in *. destruct H3. subst x. apply impl; auto.
Defined.

Definition IO_raw_projT1 :
  forall
    {pre} {X : Set} {A : X -> Set} {post_xa}
    (ma : IO pre (sigS A) post_xa)
    (post_x : X -> list w.msg -> list w.msg -> Prop)
    (impl : forall xa s t, post_xa xa s t -> post_x (projT1 xa) s t),
    IO pre X post_x.
Proof.
  intros. refine (IO_raw_bind _ ma _ (fun xa => IO_raw_return (projT1 xa)) _ _ _).
  - auto.
  - auto.
  - intros. destruct H3; subst. simpl. apply impl. assumption.
Defined.

Definition IO_raw_projT2 :
  forall
    {pre} {X : Set} {A : Set} {post_xa}
    (ma : IO pre (sigS (fun (_ : X) => A)) post_xa)
    (post_a : A -> list w.msg -> list w.msg -> Prop)
    (impl : forall xa s t, post_xa xa s t -> post_a (projT2 xa) s t),
    IO pre A post_a.
Proof.
  intros. refine (IO_raw_bind _ ma _ (fun xa => IO_raw_return (projT2 xa)) _ _ _).
  - auto.
  - auto.
  - intros. destruct H3; subst. simpl. apply impl. assumption.
Defined.

End IORaw2IORaw0.
