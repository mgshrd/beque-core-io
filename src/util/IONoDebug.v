Require Import World.
Require Import IORaw.
Require Import IOAux.

(* Keep the debug notation, but don't use the specified tracing messages *)
Module IONoDebug
       (w : World)
       (ior : IORaw w)
.

Module io := IOAux w ior.
Import io.
Export io.

Set Warnings "-non-reversible-notation".
Notation "/|? msg \ m" := (/| m) (at level 30).
Notation "|\? msg \ m" := (|\ m) (at level 30).

Notation "a <--? msg \ ma ;; e" := (b <-- ma ;; e) (at level 30, ma at next level, right associativity).

Notation "'do?' msg \ x" := (do x) (at level 30, right associativity).
Notation "'dou?' msg \ x" := (dou x) (at level 30, right associativity).

End IONoDebug.
