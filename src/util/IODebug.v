Require String.

Require Import result.

Require Import World.
Require Import IORaw.
Require Import IOAux.

Require Import ResultUtils.

Module IODebug
       (w : World)
       (ior : IORaw w)
.

Module io := IOAux w ior.
Import io.
Export io.
Import ior.
Export ior.

Module ru := ResultUtils w ior.
Import ru.

(******************************* String marker *****************************************)
Definition marker_inhabits_string := @inhabits String.string.
Arguments marker_inhabits_string x%string_scope.
Inductive mark_prop (name : inhabited String.string) : Prop := marker_ctor.
Notation "'mark' a" := (mark_prop (marker_inhabits_string a)) (at level 90).

Definition inhapp (s1 s2 : inhabited String.string) : inhabited String.string.
Proof.
  destruct s1, s2. exact (inhabits (String.append H H0)).
Defined.
Arguments inhapp (s1 s2)%string_scope.

Definition inhapps (is : inhabited String.string) (s : String.string) : inhabited String.string.
Proof.
  destruct is. exact (inhabits (String.append H s)).
Defined.
Arguments inhapps _ s%string_scope.
Notation "'marks' a b" := (inhapps (marker_inhabits_string a) b) (at level 90).

(******************************* String marker end *****************************************)

Definition IO_debug_strengthen :
  forall
    (name : inhabited String.string) (*eliminate at extraction*)
    {pre_a A post_a}
    (ma : IO pre_a A post_a)
    {pre_a' : list w.msg -> Prop}
    (impl_pre : forall t, mark_prop name /\ (pre_a' t -> pre_a t)),
    IO pre_a' A post_a.
Proof.
  intros.
  assert (Hx := @IO_strengthen _ _ _ ma pre_a').
  apply Hx. intros. destruct (impl_pre t).
  apply H1. exact H.
Defined.
Arguments IO_debug_strengthen _%string_scope _ _ _ _ _ _.

Definition IO_debug_weaken
           (name : inhabited String.string) (*eliminate at extraction*)
           {pre_a A post_a}
           (ma : IO pre_a A post_a)
           {post_a' : A -> list w.msg -> list w.msg -> Prop}
           (impl_post : forall a s t, (mark_prop name /\ pre_a t) -> post_a a s t -> post_a' a s t) :
  IO pre_a A post_a'.
Proof.
  assert (Hx := @IO_weaken _ _ _ ma post_a').
  apply Hx. intros. apply impl_post.
  - split; [ constructor | ]. exact H.
  - exact H0.
Defined.
Arguments IO_debug_weaken _%string_scope _ _ _ _ _ _.

Definition IO_debug_bind_flexpre
           (name : inhabited String.string) (*eliminate at extraction*)
           (pre : list w.msg -> Prop)
           {pre_a} {A : Set} {post_a}
           (ma : IO pre_a A post_a)
           (impl_pre :
              forall t,
                (mark_prop name /\ pre t) ->
                pre_a t)
           {B : A -> Set}
           {post_b : forall a : A, B a -> list w.msg -> list w.msg -> Prop}
           (f : forall a : A, IO (fun t => exists s t', (s ++ t')%list = t /\ pre t' /\ post_a a s t') (B a) (post_b a)) :
  IO
    pre
    (sigS B)
    (fun ab s t =>
       exists s'' s',
         (s'' ++ s')%list = s /\
         pre_a t /\
         post_a (projT1 ab) s' t /\
         post_b (projT1 ab) (projT2 ab) s'' (s' ++ t)%list).
Proof.
  refine (IO_bind_flexpre pre ma _ f).
  intros. apply impl_pre. split; [ constructor | exact H ].
Defined.
Arguments IO_debug_bind_flexpre _%string_scope _ _ _ _ _ _ _ _ _.

Definition IO_debug_pure_map
           (name : inhabited String.string) (*eliminated at extraction*)
           {pre} {A : Set} {post_a}
           (ma : IO pre A post_a)
           {B : Set}
           (*TODO(f : mark_prop (inhapps name ": map") * A -> B)*)
           (f : mark_prop name * A -> B)
           (post_b : B -> list w.msg -> list w.msg -> Prop)
           (impl :
              forall a s t,
                (*TODO(mark_prop (inhapps name ": pre") /\ pre t) ->*)
                (mark_prop name /\ pre t) ->
                post_a a s t ->
                post_b (f (marker_ctor _, a)) s t) :
  IO pre B post_b.
Proof.
  refine (IO_pure_map ma (fun a => f (marker_ctor _, a)) post_b _).
  intros. apply impl.
  - split; [ exact (marker_ctor _) | ]. exact H.
  - exact H0.
Defined.
Arguments IO_debug_pure_map _%string_scope _ _ _ _ _ _ _ _.

Definition IO_debug_drop_value
           (name : inhabited String.string) (*eliminated at extraction*)
           {pre A post_a}
           (m : IO pre A post_a)
           (post : list w.msg -> list w.msg -> Prop)
           (impl : forall a s t, (mark_prop name /\ pre t) -> post_a a s t -> post s t) :
  IO_cmd pre post.
Proof.
  refine (IO_drop_value m post _).
  intros. apply (impl a).
  - split; [ constructor | ]. exact H.
  - exact H0.
Defined.
Arguments IO_debug_drop_value _%string_scope _ _ _ _ _ _.

Notation "/|? msg \ m" := (IO_debug_strengthen (marker_inhabits_string msg) m _) (at level 30).
Notation "|\? msg \ m" := (IO_debug_weaken (marker_inhabits_string msg) m _) (at level 30).

Notation "a <--? msg \ ma ;; e" := (IO_debug_bind_flexpre (marker_inhabits_string msg) _ ma _ (fun a => e)) (at level 30, ma at next level, right associativity).

Notation "'do?' msg \ x" := (IO_debug_pure_map (marker_inhabits_string msg) (x) _ _ _) (at level 30, right associativity).
Notation "'dou?' msg \ x" := (IO_debug_drop_value (marker_inhabits_string msg) (x) _ _) (at level 30, right associativity).

Definition result_debug_bind
           (name : inhabited String.string) (*eliminate at extraction*)
           (pre : list w.msg -> Prop)
           {pre_a} {A E : Set} {post_a}
           (ma : IO pre_a (result A E) post_a)
           (impl_pre :
              forall t,
                (mark_prop name /\ pre t) ->
                pre_a t)

           {B : A -> Set}
           {post_b : forall a : A, (B a) -> list w.msg -> list w.msg -> Prop}
           (f : forall a : A, IO (fun t => exists s t', (s ++ t')%list = t /\ pre t' /\ post_a (ok a) s t') (B a) (post_b a))

           {B' : E -> Set}
           {post_e : forall e : E, B' e -> list w.msg -> list w.msg -> Prop}
           (g : forall e : E, IO (fun t => exists s t', (s ++ t')%list = t /\ pre t' /\ post_a (err e) s t') (B' e) (post_e e)) :
    IO
      pre
      (sigS (fun a : result A E =>
               match a with
               | ok a => B a
               | err e => B' e
               end))
      (fun ab s t =>
         exists s'' s',
           (s'' ++ s')%list = s /\
           pre_a t /\
           post_a (projT1 ab) s' t /\
           match projT1 ab as a return projT1 ab = a -> _ with
           | ok a' =>
             fun eqpf =>
               post_b a' ltac:(destruct ab as [ ra b ]; simpl in *; rewrite eqpf in b; exact b) s'' (s' ++ t)%list /\
               projT1 ab = ok a'
           | err e =>
             fun eqpf =>
               post_e e ltac:(destruct ab as [ ra b ]; simpl in *; rewrite eqpf in b; exact b) s'' (s' ++ t)%list /\
               projT1 ab = err e
           end eq_refl).
Proof.
  intros. refine (a <?-- ma [err g a ];; f a).
  intros. apply impl_pre. split; [ constructor | exact H ].
Defined.
Arguments result_debug_bind _%string_scope _ _ _ _ _ _ _ _ _ _ _ _ _.
Notation "a <?--? msg \ ma [ 'err' erre ] ;; oke" := (result_debug_bind (marker_inhabits_string msg) _ ma _ (fun a => oke) (fun a => erre)) (at level 30, ma at next level, right associativity).

Definition result_debug_ok_bind
           (name : inhabited String.string) (*eliminate at extraction*)
           (pre : list w.msg -> Prop)
           {pre_a} {A E : Set} {post_a}
           (ma : IO pre_a (result A E) post_a)
           (impl_pre :
              forall t,
                (mark_prop name /\ pre t) ->
                pre_a t)
           {B : A -> Set}
           {post_b : forall a : A, B a -> list w.msg -> list w.msg -> Prop}
           (f : forall a : A, IO (fun t => exists s t', (s ++ t')%list = t /\ pre t' /\ post_a (ok a) s t') (B a) (post_b a)) :
  IO
    pre
    (sigS (fun r : result A E => match r with
                                 | ok a => B a
                                 | err _ => unit
                                 end))
    (fun ab s t =>
       exists s'' s',
         (s'' ++ s')%list = s /\
         pre_a t /\
         post_a (projT1 ab) s' t /\
         match projT1 ab as a' return projT1 ab = a' -> _ with
         | ok a =>
           fun eqpf =>
             post_b
               a
               (*projT2 ab*) ltac:(destruct ab as [ ? b ]; unfold projT1 in *; rewrite eqpf in b; exact b)
               s''
               (s' ++ t)%list
         | err _ =>
           fun eqpf =>
             s'' = nil
         end eq_refl).
Proof.
  intros. refine (a <?-- ma ;; f a).
  intros. apply impl_pre. split; [ constructor | assumption ].
Defined.
Arguments result_debug_ok_bind _%string_scope _ _ _ _ _ _ _ _ _ _.
Notation "a <?--? msg \ ma ;; e" := (result_debug_ok_bind (marker_inhabits_string msg) _ ma _ (fun a => e)) (at level 30, ma at next level, right associativity).

End IODebug.
