Require Import ltac_utils.
Require Import ltac_seg.
Require Import result.

Require Import intf.World.
Require Import intf.IORaw.
Require Import intf.IOAux.


(** Helper for [sumbind]. *)
Definition res2sum {A E} (r : result A E) : E + A := match r with ok a => inr a | err e => inl e end.
Coercion res2sum : result >-> sum.
Set Printing Coercions.

(** Helpers to work with [result] in [IO] contexts. *)
Module ResultUtils
       (w : World)
       (ior : IORaw w)
.

Import ior.

Module io := IOAux w ior.
Import io.

(** Helps to [bind] to a [result] typed action. *)
Definition result_bind
           (pre : list w.msg -> Prop)
           {pre_a} {A E : Set} {post_a}
           (ma : IO pre_a (result A E) post_a)
           (impl_pre :
              forall t,
                pre t ->
                pre_a t)

           {B : A -> Set}
           {post_b : forall a : A, (B a) -> list w.msg -> list w.msg -> Prop}
           (f : forall a : A, IO (fun t => exists s t', (s ++ t')%list = t /\ pre t' /\ post_a (ok a) s t') (B a) (post_b a))

           {B' : E -> Set}
           {post_e : forall e : E, B' e -> list w.msg -> list w.msg -> Prop}
           (g : forall e : E, IO (fun t => exists s t', (s ++ t')%list = t /\ pre t' /\ post_a (err e) s t') (B' e) (post_e e)) :
    IO
      pre
      (sigS (fun a : result A E =>
               match a with
               | ok a => B a
               | err e => B' e
               end))
      (fun ab s t =>
         exists s'' s',
           (s'' ++ s')%list = s /\
           pre_a t /\
           post_a (projT1 ab) s' t /\
           match projT1 ab as a return projT1 ab = a -> _ with
           | ok a' =>
             fun eqpf =>
               post_b a' ltac:(destruct ab as [ ra b ]; simpl in *; rewrite eqpf in b; exact b) s'' (s' ++ t)%list /\
               projT1 ab = ok a'
           | err e =>
             fun eqpf =>
               post_e e ltac:(destruct ab as [ ra b ]; simpl in *; rewrite eqpf in b; exact b) s'' (s' ++ t)%list /\
               projT1 ab = err e
           end eq_refl).
Proof.
  unshelve refine (do
            ra <-- ma;;
            sumbind _ ra
            (fun a => /| f a)
            (fun e => /| g e)); try solve [ simpl; auto ].

  {
    destruct v. exists x. destruct x; exact y.
  }

  {
    intros. sega. exists x. exists x0.
    destruct ra; [ | discriminate H0 ].
    repeat split; auto.
    inversion H0. subst a0. exact H1.
  }

  {
    intros. sega. exists x. exists x0.
    destruct ra; [ discriminate H0 | ].
    repeat split; auto.
    inversion H0. subst e0. exact H1.
  }

  {
    intros. desig. sega. exists x0. exists x1. simpl in *.
    destruct x; simpl in *.
    all: tauto.
  }
Defined.
Notation "a <?-- ma [ 'err' erre ] ;; oke" := (result_bind _ ma _ (fun a => oke) (fun a => erre)) (at level 30, ma at next level, right associativity).

(** Helps to [bind] to the success part of a [result] typed action.

Similar to Rust's early return '?' operator. *)
Definition result_ok_bind
           (pre : list w.msg -> Prop)
           {pre_a} {A E : Set} {post_a}
           (ma : IO pre_a (result A E) post_a)
           (impl_pre :
              forall t,
                pre t ->
                pre_a t)
           {B : A -> Set}
           {post_b : forall a : A, B a -> list w.msg -> list w.msg -> Prop}
           (f : forall a : A, IO (fun t => exists s t', (s ++ t')%list = t /\ pre t' /\ post_a (ok a) s t') (B a) (post_b a)) :
  IO
    pre
    (sigS (fun r : result A E => match r with
                                 | ok a => B a
                                 | err _ => unit
                                 end))
    (fun ab s t =>
       exists s'' s',
         (s'' ++ s')%list = s /\
         pre_a t /\
         post_a (projT1 ab) s' t /\
         match projT1 ab as a' return projT1 ab = a' -> _ with
         | ok a =>
           fun eqpf =>
             post_b
               a
               (*projT2 ab*) ltac:(destruct ab as [ ? b ]; unfold projT1 in *; rewrite eqpf in b; exact b)
               s''
               (s' ++ t)%list
         | err _ =>
           fun eqpf =>
             s'' = nil
         end eq_refl).
Proof.
  intros.
  unshelve refine (do
            a <?-- ma
            [err /| IO_return a ];;
            /| f a
         ); try solve [ simpl; auto ].

  {
    exists (projT1 v).
    destruct v; simpl in *.
    destruct x as [ ? | e ].
    - exact y.
    - exact tt.
  }

  {
    intros. desig. sega. exists x0. exists x1. simpl in *.
    repeat split; try tauto.
    destruct x.
    all: try solve [ apply H2 ].
  }
Defined.
Notation "a <?-- ma ;; e" := (result_ok_bind _ ma _ (fun a => e)) (at level 30, ma at next level, right associativity).

End ResultUtils.
