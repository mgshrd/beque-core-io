Require Import List.

Require Import result.
Require Import dec_utils.
Require Import inj_pair2b.
Require Import ltac_lens.
Require Import ltac_utils.
Require Import ltac_seg.

Require Import World.
Require Import IORaw.
Require Import IOAux.
Require Import ResultUtils.

Module IOAuxDemo
       (w : World)
       (ior : IORaw w)
.

Import ior.
Module io := IOAux w ior.
Import io.

Axiom A : Set.
Axiom B : Set.
Axiom eq_B_dec : forall b1 b2 : B, {b1=b2}+{b1<>b2}.
Axiom C : Set.
Axiom PX : list w.msg -> Prop.
Axiom PA : A -> list w.msg -> Prop.
Axiom PB : B -> list w.msg -> list w.msg -> Prop.
Axiom PB' : B -> list w.msg -> Prop.
Axiom PB'PB : forall b s t, PB b s t -> PB' b (s ++ t).
Axiom PC : C -> list w.msg -> list w.msg -> Prop.
Axiom openx :
  forall a : A, IO (fun _ => True) B (fun b s t => PA a (s ++ t) /\ PB b s t).
Axiom openxr :
  forall a : A, IO (fun _ => True) (result B nat) (fun b s t => PA a (s ++ t) /\ match b with
                                                                                   | ok b => PB b s t
                                                                                   | err ec => True
                                                                                 end).
Axiom dox :
  forall b : B, IO (fun _ => True) C (fun c s t => PC c s t).
Axiom doynp :
  forall b : B, IO (fun t => True) C (fun c s t => PC c s t).

Axiom doy :
  forall b : B, IO (fun t => exists s t', s ++ t' = t /\ PB' b t') C (fun c s t => PC c s t).
Axiom closex :
  forall b c, IO (fun t => exists s'5 s'4 s''' s'' s' t',
                             s'5 ++ s'4 ++ s''' ++ s'' ++ s' ++ t' = t /\
                             PB b s'' (s' ++ t') /\
                             PC c s'4 (s''' ++ s'' ++ s' ++ t')) unit (fun b s t => True).

Goal
  A -> IO
         (fun t => PX t)
         (result (B * C) nat)
         (fun bc s t =>
            exists s'5 s'4 s''' s'' s',
              s'5 ++ s'4 ++ s''' ++ s'' ++ s' = s /\
              PX t /\
              match bc with
              | ok bc =>
                1 = 1 /\
                PB (fst bc) s'' (s' ++ t) /\
                PC (snd bc) s'4 (s''' ++ s'' ++ s' ++ t)
              | err _ => True
              end).
Proof.
  intros a.

  unshelve refine (
      do
        _ <-- IO_return tt;;
        eb <== (
          _ <-- IO_return 0;;
            eb <-- openxr a;;
            _ <-- IO_return 1;;
            _ <-- IO_return tt;;
            /| IO_return eb
        );;
        eb <-- IO_return (focuslast eb);;
        cyy <==
        sumbind _ eb
        (fun b =>
           c <== (
               _ <-- IO_return tt;;
                 c <-- doy b;;
                 _ <-- IO_return c;;
                 /| IO_return 0
             );;
             /| IO_return (focusto C inside c))
        (fun _ => /| IO_return tt);;
        sumbindsig _ (existT _ (eb:sum _ _) cyy)
        (fun bc =>
           /| closex (fst bc) ((projT2 (snd (bc)))))
        (fun e_ => /| IO_return (fst e_))
    ); try solve [ intros; simpl in *; auto ].

  {
    desig. destruct x1; simpl in *.
    - desig. exact (ok (b, c)).
    - exact (err y).
  }

  {
    intros. sega. simpl in *. subst.
    destruct x0; [ | discriminate H1 ].
    simpl in H1. inversion H1. subst b0. clear H1.
    exists nil. exists (x9 ++ x12).
    split; [ reflexivity | ].
    apply PB'PB. assumption.
  }

  {
    intros. destruct bc. sega. simpl in *. destruct r; simpl in *; [ | discriminate ].
    desig; simpl in *. inversion H0. subst b0.
    set (eq_rd := eq_sum_dec PeanoNat_Nat_eq_dec eq_B_dec). set (Hx := eq_existT_projT2 _ eq_rd _ (inr b) _ _ H0).
    inversion Hx. subst. clear Hx eq_rd H0.
    sega. exists nil. exists x6. exists nil. exists x12. exists nil. exists x15. tauto.
  }

  {
    intros. desig. simpl in *. destruct x1.
    - simpl in *. desig. simpl in *. sega.
      exists x9. exists x15. exists nil. exists x36. exists nil. rewrite app_nil_r. tauto.
    - exists nil. exists nil. exists nil. exists nil. exists s. tauto.
  }

Qed.

End IOAuxDemo.
